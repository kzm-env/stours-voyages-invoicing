from odoo import models, fields, _
from odoo.http import request

from odoo.exceptions import UserError
import logging
import requests
import json
from datetime import datetime


_logger = logging.getLogger(__name__)


class ValidateAccountMoveSengerio(models.TransientModel):
    _inherit = "validate.account.move"

    def read_invoice(self, invoice_id):
        url = "https://api.sengerio.com/v1/invoice?id="
        url = url + str(invoice_id)
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer  YOUR TOKEN",
        }
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            return ""
        print(response)
        response = response.json()
        data = response["data"]
        # _logger.info(f"type data : {type(data)}, data : {data}")
        current_invoice_name = data["number"]
        if not self.check_for_invoice(invoice_name=current_invoice_name):
            # Get the current date
            current_date = datetime.now().date()

            # Convert the current date to string format
            current_invoice_date = current_date.strftime("%Y-%m-%d")
            move_data = {
                "name": current_invoice_name,
                "move_type": "out_invoice",
                "invoice_date": current_invoice_date,
            }
            invoice = request.env["account.move"].create(move_data)
            # totalInvoice

            invoice_rows = data["totalInvoice"]["invoiceRows"]

            # create product :
            product_list_ids = []
            for invoice_row in invoice_rows:
                product_name = invoice_row["description"]
                product_price = invoice_row["unit_price"]
                quantity = invoice_row["quantity"]
                product_data = dict(name=product_name, list_price=product_price)
                product = request.env["product.product"].create(product_data)
                product_list_ids.append(product["id"])
                # _logger.info(
                #     f"invoice_row : {type(invoice_rows)}, invoice_row : {invoice_row}"
                # )
                invoice_line_data = dict(
                    move_id=invoice["id"],
                    display_type="product",
                    product_id=product["id"],
                    quantity=quantity,
                )
                invoice_line = request.env["account.move.line"].create(
                    invoice_line_data
                )

            # )

    def check_for_invoice(self, invoice_name):
        _logger.info(f"check_for_invoice : {invoice_name}")

        invoice = request.env["account.move"].search([("name", "=", invoice_name)])
        if invoice:
            data = invoice["id"]

            # _logger.info(f"type data : {type(invoice)}, data : {data}")
        return invoice

    def validate_sengerio_invoices(self):
        # print(f"ana han   _________")
        # _logger.info("def validate_sengerio_invoices(self):  ----")
        url = "https://api.sengerio.com/v1/customer-invoices"
        headers = {
            "Content-Type": "application/json",  # Example header
            "Authorization": "Bearer YOUR TOKEN",  # Example authorization header
        }
        response = requests.get(url, headers=headers)

        if response.status_code == 200:
            # print(response)
            response = response.json()
            # _logger.info(f"type response : {type(response)}, response : {response}")

            rows = response["rows"]

            for row in rows:
                self.read_invoice(row["id"])

        else:
            print(f"Error: {response.status_code}")
        return {"type": "ir.actions.act_window_close"}
