# -*- coding: utf-8 -*-
{
    "name": "Stours Voyages Invoicing",
    "version": "0.1",
    
    "summary": "Stours Voyages Invoices & Payments",
    "description": """Stours Voyages Invoicing & Payments """,
    "category": "Accounting/Accounting",
    "website": "",
    "depends": ["base_setup", "account"],
    "data": ["views/account_syncron_button.xml"],
    "installable": True,
    "application": True,
    "license": "LGPL-3",
}
